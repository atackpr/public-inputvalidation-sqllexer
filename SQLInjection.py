
from SQLLexer import *

class SQLInjection():

	def __init__(self):
			self.lexer = SQLLexer()
			self.lexer.build()

			self.u_tok_counter = None
			self.s_tok_counter = None


	def validateLex(self, sample_sql, user_sql):

		self.s_tok_counter = self.lexer.getTokensHash()
		self.u_tok_counter = self.lexer.getTokensHash()

		for tok in self.lexer.tokenize(sample_sql):
	 		self.s_tok_counter[tok.type] += 1

	 	for tok in self.lexer.tokenize(user_sql):
	 		self.u_tok_counter[tok.type] += 1

	 	return self.s_tok_counter == self.u_tok_counter

	def getLastTokCounters(self):
		return self.s_tok_counter, self.u_tok_counter


if __name__ == '__main__':

	sqlI = SQLInjection()

	# Test 1
	print sqlI.validateLex("""select cat from dog where casa=1 ;""", """select cat from dog where casa=1 ;""")

	# Test 2
	print sqlI.validateLex("""select cat from dog where casa=1 ;""", """select cat from dog where casa=1 and cat="miau" ;""")
