from SQLInjection import *

if __name__ == '__main__':

	sqlI = SQLInjection()

	# Test 1
	sample = """select cat from dog where casa=1 ;"""
	u_input =  """select cat from dog where casa=2 ;"""

	print "Test 1"
	print "Sample:  ", sample
	print "User In: ", u_input
	print "Is Valid?: ", sqlI.validateLex(sample, u_input)
	print

	# Test 2

	u_input = """select cat from dog where casa=1 and cat="miau" ;"""
	print "Test 1"
	print "Sample:  ", sample
	print "User In: ", u_input
	print "Is Valid?: ", sqlI.validateLex(sample, u_input)
	print

	# Interactive Example with user input

	print "Follow the instruction and then try to inject SQL."

	while True:
		try: 
			s = raw_input("Input a number> ")
		except EOFError:
			break

		u_input = """select cat from dog where casa=%s ;""" %s
		print "User query: %s" % u_input

		try:
			print "Is Valid?: ", sqlI.validateLex(sample, u_input)
			s_counter, u_counter = sqlI.getLastTokCounters()
			print "Token NUMBER  sample: %s\t user:%s" % (s_counter["NUMBER"], u_counter["NUMBER"])
			print "Token ID      sample: %s\t user:%s" % (s_counter["ID"], u_counter["ID"])
			print "Token LITERAL sample: %s\t user:%s" % (s_counter["LITERAL"], u_counter["LITERAL"])
			print "Token AND+OR  sample: %s\t user:%s" % (s_counter["AND"] + s_counter["OR"], u_counter["AND"] + u_counter["OR"])
			print 
		except:
			print "False"




