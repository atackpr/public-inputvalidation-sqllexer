[English](#markdown-header-regular-expressions-sql-injection) | [Español](#markdown-header-expresiones-regulares-inyeccion-de-sql)

# Expresiones Regulares - Inyección de SQL
##Objetivos

1. Practicar la implementación de expresiones regulares para construir un analizador lexicografico de una versión simple de una pregunta de selección de SQL.
 
## Pre-Modulo:

### Bajar PLY
Obten PLY desde: http://www.dabeaz.com/ply/

###Instalar PLY

Descomprime el archivo:

```
tar xzvf ply-X.X.tar.gz
```

Entra al directorio creado:

```
cd ply-X.X
```

Construye los modulos de PLY:

```
python setup.py build
```

Instala PLY en el sistema como administrador:
```
sudo python setup.py install
```

### Pre-Modulo

Antes de trabajar en este módulo el estudiante debe

1. Haber repasado la construcción de expresiones regulares.
2. Haber repasado la implementación de expresiones regulares en python.
3. Haber leido sobre el constructor de analizadores lexicográficos lex.

## Prevenir Inyección de SQL usando un analizador lexicográfico

La validación de entradas es escencial en el desarrollo de aplicaciones seguras ya que es la primera línea de defensa de todas las aplicaciones.  La gran mayoría de las vulnerabilidades en aplicaciones es debido a pobre validación de las entradas de las aplicaciones. Ejemplo de vulnerabilidades explotadas por pobre validación de entradas lo son:

1. Desbordamiento del Búfer 
2. Cross Site Scripting
3. Inyección de SQL, XML u otros lenguajes
4. Recorrido de directorios
5. Acceso a archivos privados

Los atacantes utilizan errores en la validación de entradas para acceder a información privilegiada, para acceder a los sistemas, o para causar negación de servicios.

Utilizando un analizador lexicográfico se puede comparar las fichas generadas por una pregunta de SQL válida con el tipo de entradas esperadas versus las fichas generadas por preguntas de SQL con entradas de usuario y determinar por la diferencia en la cantidad de fichas generadas si la entrada del usuario es válida.

Un ejemplo de una pregunta de SQL sencilla es:

```
select * from infoprivada where contrasena="valor aleatorio desconocido" ;
```

Esta pregunta de SQL revelaría la información "privada" de un campo de una tabla si se provee la contraseña correcta.  En la aplicación donde se utiliza esta pregunta la entrada del usuario consiste de insertar la contraseña.  Un posible ataque que revelaría la información privada de toda la tabla sería que usuario insertara como valor la cadena de caracteres `1" or 1=1` que generaría la pregunta: 

```
select * from infoprivada where contrasena="1" or 1=1;
```

En cuyo caso por la lógica de la pregunta la expresión del WHERE es siempre cierta, y por lo tanto revelaría toda la información de la tabla. Para aprender más sobre Inyección de SQL vaya a la referencia[1].

Para propósito de este módulo, el analizador lexicográfico que se va a construir sirve para obtener las fichas de una versión simplificada de la pregunta de selección de SQL cuyas fichas son las que siguen:

SELECT, FROM, WHERE, AND, OR, NUMBER, ID, LITERAL, LPAREN `(`, RPAREN `)`, EQ, NEQ, GT, LT, GTE, LTE, SEMI, COMMA, WILD `*`, COMMENT

Por ejemplo:

Para la pregunta de SQL `select * from table where field=%s`, donde %s sería la entrada del usuario; la posible entrada que requiere la aplicación donde se está utilizando puede ser un número insertado por el usuario. 

Como la entrada esperada es un número, una pregunta que se conoce que va a ser válida es `select * from table where field=1`, la cual generaría las fichas:

SELECT, WILD, FROM, ID, WHERE, ID, EQ, NUMBER

Si un usuario entrara cualquier otro número lo cual es válido, también se generarían las fichas 

SELECT, WILD, FROM, ID, WHERE, ID, EQ, NUMBER

Pero si el usuario en ves de entrar un número ingresara la entrada `1 or 1=1` para que la pregunta siempre sea cierta `select * from table where field=1 or 1=1` las fichas que se generarían son:

SELECT, WILD, FROM, ID, WHERE, ID, EQ, NUMBER, OR, NUMBER, EQ, NUMBER

que como se puede observar son diferente a la cantidad de fichas generadas por una pregunta válida ya que hay 2 fichas NUMBER y una ficha EQ más que en el número de fichas en un query válido.

Otro ejemplo de una entrada inválida sería poner algo diferente a un número como una cadena de caracteres como myfield `select * from table where field=myfield` donde las fichas generadas serían:

SELECT, WILD, FROM, ID, WHERE, ID, EQ, ID

que como se puede observar la cantidad de fichas es la misma pero las fichas difieren en la última ficha.

**Descargo de responsabilidad** Esto es un módulo cuyo proposito es enseñar expresiones regulares y construcción de analizadores lexicográficos utilizando una aplicación de seguridad.

## Instrucciones generales

En este módulo utilizaremos la librería de python PLY para construir un analizador lexicográfico de preguntas de selección de SQL simples.  El estudiante construirá expresiones regulares para completar el analizador lexicográfico y luego seguirá instrucciones para aprender como el analisis lexicográfico sirve para prevenir inyecciones de SQL.

El archivo que el estudiante modificará para llevar a cabo este módulo es:
`SQLLexer.py`


## Ejercicio 1: Expresiones regulares para completar el analizador lexicográfico.

Edite el archivo `SQLLexer.py` para completar las expresiones regulares en la sección que dice  # REGULAR EXPRESSION RULES FOR TOKENS

1. Para `number` la expresión regular es uno o mas digitos.
2. Para `identifier` la expresión regular empieza con un caracter del alfabeto o un subrayar (_), seguido por zero o mas caracteres del alfabeto, digitos o subrayares.
3. Para `literal` la expresión regular comienza con comillas dobles, seguido por cualquier número de artículos que:
    1. no son comillas dobles o barras invertida (\\)
    2. o son una barra invertida seguida por cualquier caracter

    y termina con comillas dobles.

4. Para `comment` la expresión regular comienza con dos guiones, seguido por cualquier numero de caracteres.

## Ejercicio 2: Correr el analizador lexicografico para contestar preguntas

Corra el analizador lexicografico:
```
python  InjectionLexTest.py
```
Van a salir desplegadas dos pruebas donde se comparan una pregunta válida de ejemplo y otra donde las entradas del WHERE es distinta.

1. Note que para la prueba 1, el resultado es válido.  Anote por qué?
2. Note que para la prueba 2, el resultado es inválido.  Anote por qué?
3. Inserte como entrada un número entero cualquiera. Es válido?  Por qué?
4. Inserte como entrada `1 or 1=1`.  Es válido? Por qué?
5. Inserte como entrada `"ccom" or casa=4`.  Es válido? Por qué?

## Ejercicio 3: Un analizador léxico para preguntas de SQL para insertar

Modifica el analizador léxico de el archivo `SQLLexer.py` para también analizar el léxico de preguntas de SQL simples como las siguientes: 

insert into table (col1, col2, col3) value (id, 3, "literal") ;
insert into table (col1, col2, col3) values (id, 3,"literal"), (id, 4, "otherliteral") ;

**Nota** Crea una copia de el archivo `SQLLexer.py` del Ejercicio 1 porque necesitaras entregarlo al instructor con otra copia del `SQLLexer.py` para este ejercicio. 

## Entregas

Entregue al instructor una copia del archivo `SQLLexer.py` del Ejercicio 1 y el Ejercicio 3, y las contestaciones a las preguntas del Ejercicio 2.

## References:
[1] https://en.wikipedia.org/wiki/SQL_injection

--- 

[English](#markdown-header-regular-expressions-sql-injection) | [Español](#markdown-header-expresiones-regulares-inyeccion-de-sql)

# Regular Expressions - SQL Injection

##Objectives

1. Practice the implementation of regular expressions to construct a lexical analyzer of a simple version of a SQL select query.
 
## Pre-Module:

### Download PLY
Get PLY from: http://www.dabeaz.com/ply/

###Install PLY

Uncompress the file:

```
tar xzvf ply-X.X.tar.gz
```

Enter the directory created:

```
cd ply-X.X
```

Construct the PLY modules:

```
python setup.py build
```

Install PLY in the system as an administrator:

```
sudo python setup.py install
```

### Pre-Module

Before working in this module the student must

1. Have reviewed the construction of regular expressions.
2. Have reviewed the implementation of regular expressions in python.
3. Have read about the constructor of lexical analyzers lex.

## Prevention SQL injection using a lexical analyzer

Input validation is esential in the development of secure applications because it is the first line of defense of every application.  The vast majority of vulnerabilities in applications is due to poor input validation of the applications.  Example of vulnerabilities explited by poor input validation are:

1. Buffer overflows
2. Cross Site Scripting
3. SQL, XML or other languages injection
4. Directory Traversal
5. Access to private files

The attackers use errors in input validation to gain access to priviledged information, to gain access to systems, or to cause denial of services.

Using a lexical analyzer we can compare the tokens generated by a valid SQL query with expected inputs types versus the tokens generated by a SQL query with user input and determine if the user input is valid by the differences in the amount of tokens. 

An example of a simple SQL query is:

```
select * from infoprivada where contrasena="valor aleatorio desconocido" ;
```

This SQL query would reveal "private" information of a field of the a table if the right password is provided.  In the aplication where this query would be used the user input would consist of inserting the password.  A possible atack that would reveal the private information of all the table would be that the user inserts as a value the string `1" or 1=1` that would generate the query:

```
select * from infoprivada where contrasena="1" or 1=1;
```

In which case because of the logic of the query WHERE expression is always true, and therefore it would reveal all the information in the table.  To learn more about SQL injection go to reference [1].

For the purpose of this module, the lexical analyzer that will be constructed will serve to obtain the tokens of a simplified version of the select SQL query which tokens are the following:

SELECT, FROM, WHERE, AND, OR, NUMBER, ID, LITERAL, LPAREN `(`, RPAREN `)`, EQ, NEQ, GT, LT, GTE, LTE, SEMI, COMMA, WILD `*`, COMMENT

For example:

For the SQL query `select * from table where field=%s`, where %s would be the user input; the possible input requiered by the application where the query is being used could be a number inserted by the user.

Because the expected input is a number, a query known to be valid is `select * from table where field=1`, which would generate the tokens:

SELECT, WILD, FROM, ID, WHERE, ID, EQ, NUMBER

If the user enters any other number, which is valid, also the following tokens would be generated:

SELECT, WILD, FROM, ID, WHERE, ID, EQ, NUMBER

But if the user instead of inserting a number inserts as input `1 or 1=1` to make the query always true `select * from table where field=1 or 1=1` the tokens that would be generated are:

SELECT, WILD, FROM, ID, WHERE, ID, EQ, NUMBER, OR, NUMBER, EQ, NUMBER

that as can be observed are different to the number of tokens generated by a valid query because there are two additional NUMBER tokens and one additional EQ token than in the number of tokens in the valid query.

Another example of an invalid input would be to insert something different to a number such a string like myfield `select * from table where field=myfield` where the generated tokens would be:

SELECT, WILD, FROM, ID, WHERE, ID, EQ, ID

that as can be observed the number of tokens is the same but the tokens differ in the last token.

**Disclaimer** This is a module which purpose is to teach regular expressions and to construct lexical analyzers using security as an application.

## General Instructions

In this module we will use the python library PLY to construct a lexical analyzer of simple selection SQL queries.  The student will construct regular expressions to complete the lexical analizer and then will follow instructions to learn how the lexical analyisis serves to prevent SQL injections.

The file that the student will modify to accomplish this module is: `SQLLexer.py`


## Exercise 1: Regular expression to complete the lexical analyzer

Edit the file  `SQLLexer.py` to complete the regular expression in the secctions that says: # REGULAR EXPRESSION RULES FOR TOKENS

1. For `number` the regular expression is one or more digits.
2. For `identifier` the regular expression starts with an alphabet character or an underscore followed by zero or more alphabet characters, digits or underscores.
3. For `literal` the regular expression starts with a double quote, followed by any number of items which are:
    1. either not a double-quote or a backslash
    2. or are a backslash followed by any single character
    
    and then finishes with a closing double-quote.

4. For `comment` the regular expression start with two dashes, followed by any number of characters.

## Exercise 2: Run the lexical analyzer and answer the questions

Run the lexical analyzer:

```
python  InjectionLexTest.py
```
The comparison between two tests, one with a sample valid query and other with a different WHERE expressions input is displayed.

1. Note that for test 1, the result is valid.  Why?
2. Note that for test 2, the result is invalid. Why?
3. Insert as input any integer number.  Is valid? Why?
4. Insert as input `1 or 1=1`. Is valid? Why?
5. Insert as input `"ccom" or casa=4`. Is valid? Why?
``

## Exercise 3: A lexical analyzer for insert queries

Modify the lexical analyzer of file `SQLLexer.py` to also analyze the lexicon of simple insert SQL queries like the following:

insert into table (col1, col2, col3) value (id, 3, "literal") ;
insert into table (col1, col2, col3) values (id, 3,"literal"), (id, 4, "otherliteral") ;

**Note** make a copy of the file `SQLLexer.py` from Exercise 1 because you need to give it to the instructor with another `SQLLexer.py` for this exercise. 

## Deliverables

Give the instructor a copy of the file `SQLLexer.py` from Exercise 1, and Exercise 3; and the answers to the questions from Exercise 2.

## References:
[1] https://en.wikipedia.org/wiki/SQL_injection

